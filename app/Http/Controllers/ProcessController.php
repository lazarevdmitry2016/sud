<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\FileGenerator;

class ProcessController extends BaseController
{
    //
    private $file_types=[
        'docx',
        'odt',
        'rtf',
        'html',
        // 'pdf'
    ];

    public function index(Request $request)
    {
        $document = $request['document'];
        // Принять запрос
        // вытащить номер документа и другие параметры
        $file_type=$request['file_type'];
        // dd($request);
        // для номера документа запустить соответствующий генератор файлов и передать ему параметры
        if (in_array($file_type,$this->file_types) && $document >= 0 && $document < 18){
            // получить полный путь к файлу
            $output_file=FileGenerator::generateFile($document,$request);

            if ($output_file != ''){
                // передать этот путь в ответе
                return response()->download(
                    $output_file,
                    "output.$file_type",
                    ["Content-Disposition" => "attachment; filename='output.$file_type'"]
                );
            } else {
                return "no such file";
            }
        } else {
            return "no such file type";
        }
    }
}
