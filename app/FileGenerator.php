<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileGenerator extends Model
{

    public static function generateFile($doc, $request)
    {
        $doc_type=$request['file_type'];
        $doc_number = $request['document'];
        switch($doc_type){
            case 'docx':$type='Word2007';break;
            case 'odt':$type='ODText';break;
            // case 'rtf':$type='RTF';break;
            // case 'html':$type='HTML';break;
            // case 'pdf':$type='PDF';break;
            default:return '';
        }
        $template_file=env('SUD_TEMPLATE_DIR')."Template{$doc_number}.".$doc_type;
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($template_file);
        $r=$request->all();
        $keys=array_keys($r);
        // dd($keys);
        // $keys[]="gender";
        $values=array_values($r);
        // dd($values);

        $templateProcessor->setValue($keys, $values);
        $path=env('SUD_OUTPUT_DIR').hash('sha256',microtime()).".".$doc_type;

        $templateProcessor->saveAs($path);
        return $path;

    }


}
