<?php
require_once('partials/header.php');
?>
    <div id="app">
        <div class="sidebar sidebar-theme" id="tabs">
            <ul class="sidebar-theme">
                <li><a href="#sud">Суд</a></li>
                <li><a href="#zayavitel">Заявитель</a></li>
                <li><a href="#storona">Другая сторона</a></li>
                <li><a href="#others">Прочее</a></li>
            </ul>
            <button type="button" name="button" >
                <i class="fa fa-cog" aria-hidden="true"></i>
            </button>
            <form id="inputForm" action="/process" method="post">
                <input type="hidden" name="document" v-model="doc_num">
                <!-- Суд -->
                <div id="sud">
                    <text-field
                        v-model="sud_name"
                        name="sud_name"
                        title="Наименование суда"
                        v-on:input-text="sud_name = $event">
                    </text-field>

                    <text-field
                        v-model="isk_predmet"
                        name="isk_predmet"
                        title="Предмет иска"
                        v-on:input-text="isk_predmet = $event">
                    </text-field>

                    <date-field
                        v-model="isk_reshenie_date"
                        name="isk_reshenie_date"
                        title="Дата решения суда"
                        v-on:input-date="isk_reshenie_date = $event">
                    </date-field>

                    <text-field
                        v-model="isk_result"
                        name="isk_result"
                        title="Результат решения суда"
                        v-on:input-text="isk_result = $event">
                    </text-field>
                </div>
                <!-- /Суд -->
                <!-- Заявитель -->
                <div id="zayavitel">
                    <name-field
                        v-model="zayavitel_name"
                        name="zayavitel_name"
                        title="Ф.И.О. заявителя"
                        case="i"
                        v-on:input-name="zayavitel_name = $event['in'];zayavitel_name_im = $event['out'];pol=detectGender($event['gender'])">
                    </name-field>
                    <input type='hidden' name='gender' v-bind:value="gender" />
                    <input type='hidden' name='pol' v-bind:value="pol" />
                    <input type='hidden' name='zayavitel_fio_im' v-bind:value="zayavitel_fio_im" />

                    <address-field
                        name="zayavitel_address_type"
                        v-model="zayavitel_address_type"
                        v-on:address-change="zayavitel_address_type = $event">
                    </address-field>

                    <text-field
                        v-model="zayavitel_address"
                        name="zayavitel_address"
                        title="Адрес заявителя"
                        v-on:input-text="zayavitel_address = $event">
                    </text-field>

                    <tel-field
                        v-model="zayavitel_phone"
                        name="zayavitel_phone"
                        title="Телефон заявителя"
                        v-on:input-tel="zayavitel_phone = $event">
                    </tel-field>

                    <email-field
                        v-model="zayavitel_email"
                        name="zayavitel_email"
                        title="E-mail заявителя"
                        v-on:input-email="zayavitel_email = $event">
                    </email-field>

                    <status-field
                        name="zayavitel_status"
                        label="Процессуальное положение заявителя"
                        v-model="zayavitel_status"
                        v-on:select-change="zayavitel_status = $event">
                    </status-field>
                </div>
                <!-- /Заявитель -->
                <!-- Другая сторона -->
                <div id="storona">
                    <name-field
                        v-model="storona_name"
                        name="storona_name"
                        title="Ф.И.О. другой стороны процесса"
                        case="i"
                        v-on:input-name="storona_name = $event['in']">
                    </name-field>

                    <address-field
                        name="storona_address_type"
                        v-model="storona_address_type"
                        v-on:address-change="storona_address_type = $event">
                    </address-field>

                    <text-field
                        v-model="storona_address"
                        name="storona_address"
                        title="Адрес другой стороны"
                        v-on:input-text="storona_address = $event">
                    </text-field>

                    <tel-field
                        v-model="storona_phone"
                        name="storona_phone"
                        title="Телефон другой стороны"
                        v-on:input-tel="storona_phone = $event">
                    </tel-field>

                    <email-field
                        v-model="storona_email"
                        name="storona_email"
                        title="E-mail другой стороны"
                        v-on:input-email="storona_email = $event">
                    </email-field>

                </div>
                <!-- /Другая сторона -->
                <!-- Прочие -->
                <div id="others">
                    <name-field
                        v-model="istetz_name"
                        name="istetz_name"
                        title="Ф.И.О. истца"
                        case="r"
                        v-on:input-name="istetz_name = $event['in'];istetz_name_rod = $event['out']">
                    </name-field>
                    <input type='hidden' name='istetz_name_rod' v-bind:value="istetz_name_rod" />

                    <name-field
                        v-model="otvetchik_name"
                        name="otvetchik_name"
                        title="Ф.И.О. ответчика"
                        case="d"
                        v-on:input-name="istetz_name = $event['in'];istetz_name_rod = $event['out']">
                    </name-field>
                    <input type='hidden' name='otvetchik_name_dat' v-bind:value="otvetchik_name_dat" />

                    <text-field
                        v-model="raskhody_vid"
                        name="raskhody_vid"
                        title="Виды расходов"
                        v-on:input-text="raskhody_vid = $event">
                    </text-field>

                    <number-field
                        v-model="raskhody_razmer"
                        name="raskhody_razmer"
                        title="Размер расходов"
                        v-on:input-number="raskhody_razmer = $event">
                    </number-field>

                    <number-field
                        v-model="raskhody_vsego"
                        name="raskhody_vsego"
                        title="Всего размер расходов"
                        v-on:input-number="raskhody_vsego = $event">
                    </number-field>
                </div>
                <!-- /Прочие -->
                <?php include_once('partials/form-submit.php'); ?>
            </form>
        </div>
        <div class="content">
            <div class="shapka">
                <p>В {{ sud_name ? sud_name : "(Название суда)" }} </p>
                <p>г. Ростова-на-Дону/Ростовской области</p>
                <p>Заявитель: {{ zayavitel_fio_im ? zayavitel_fio_im : "(Ф.И.О. заявителя)" }}</p>
                <p>Адрес {{ zayavitel_address_type ? zayavitel_address_type : "(Тип адреса)" }}:{{ zayavitel_address ? zayavitel_address : "(Адрес заявителя)" }}</p>
                <p v-if="zayavitel_phone">Телефон: {{ zayavitel_phone }}</p>
                <p v-if="zayavitel_email">E-mail: {{ zayavitel_email }}</p>
                <p>Заинтересованное (ые) лицо (ца): {{ storona ? storona : "(Ф.И.О. заинтересованного лица)" }}</p>
                <p>Адрес {{ storona_address_type ? storona_address_type : "(Тип адреса)" }}: {{ storona_address ? storona_address : "(Адрес заинтересованной стороны)" }}</p>
                <p v-if="storona_phone">Телефон: {{ storona_phone }}</p>
                <p v-if="storona_email">E-mail: {{ storona_email }}</p>
            </div>
            <div class="zagolovok">
                <h1>Заявление <br>о взыскании судебных расходов</h1>
            </div>
            <div class="text">
                <p>{{ istetz_fio ? istetz_fio : "(Ф. И. О./наименование истца)" }} (далее - Истец) обратился в суд с иском к {{ otvetchik_fio_dat ? otvetchik_fio_dat : "(Ф. И. О./наименование ответчика)" }} (далее - Ответчик) о {{ predmet_zayavleniya  ? predmet_zayavleniya : "(предмет иска)" }}. Решением суда от {{ reshenie_date ? reshenie_date : "(дата решения)" }} иск {{ isk_result ? isk_result : "(результат иска)" }}.</p>
                <p>В связи с рассмотрением дела в суде мною были понесены следующие судебные расходы: {{ vid_raskhodov ? vid_raskhodov : "(Вид расходов)" }} в размере {{ razmer_raskhodov ? razmer_raskhodov : "(размер расходов)" }}.</p>
                <p>Всего судебные расходы, понесенные мною в связи с рассмотрением дела в суде, составили {{ vsego_raskhodov ? vsego_raskhodov : "(всего расходов)" }} рублей.</p>
                <p>В соответствии со статьей 98 Гражданского процессуального кодекса РФ стороне, в пользу которой состоялось решение суда, суд присуждает возместить с другой стороны все понесенные по делу судебные расходы. В случае, если иск удовлетворен частично, судебные расходы присуждаются истцу пропорционально размеру удовлетворенных судом исковых требований, а ответчику - пропорционально той части исковых требований, в которой истцу отказано.</p>
                <p>На основании изложенного, руководствуясь статьями 88, 98, 100 Гражданского процессуального кодекса РФ, прошу суд:</p>
                <p>Взыскать с {{ storona_rod ? storona_rod : "(Ф.И.О./наименование заинтересованной стороны)" }} в мою пользу судебные расходы в размере {{ vsego_raskhodov ? vsego_raskhodov : "(Сумма расходов)" }} рублей.</p>
                <p>
                    Приложение:
                    <ul>
                        <li>1) договор на оказание юридических услуг;
                        </li>
                        <li>2) акт выполненных услуг;
                        </li>
                        <li>3) документ, подтверждающий оплату услуг представителя;
                        </li>
                        <li>4) документ, подтверждающий стоимость и оплату экспертизы;
                        </li>
                        <li>5) расчет стоимости судебных расходов;
                        </li>
                        <li>6) иные документы, подтверждающие стоимость и обоснованность судебных расходов.
                        </li>
                    </ul>
                </p>
            </div>
            <div class="podval">
                <div class="container">
                    <div class="grid-x">
                        <div class="large-6 medium-6 small-6 ">«____» __________20__г.</div>
                        <div class="large-6 medium-6 small-6 ">
                            <div class="text-right">_______________</div>
                            <div class="text-right">(подпись)</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        data={
            name:'',
            doc_num:'2',
            sud_name:'',
            zayavitel_name:'',
            zayavitel_status:'',
            zayavitel_address_type:'',
            zayavitel_address:'',
            zayavitel_phone:'',
            zayavitel_email:'',
            predmet_zayavleniya:'',
            storona_name:'',
            storona_name_rod:'',
            storona_address_type:'',
            storona_address:'',
            storona_phone:'',
            storona_email:'',
            istetz_name:'',
            otvetchik_name_dat:'',
            isk_predmet:'',
            isk_reshenie_date:'',
            isk_result:'',
            raskhody_vid:'',
            raskhody_razmer:'',
            raskhody_vsego:''
        };
    </script>
<?php
require_once('partials/footer.php');
