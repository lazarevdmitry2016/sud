        <footer class="social-footer">
            <div class="social-footer-left">
                <a href="/"><img class="logo" src="/images/logo.png"></a>
            </div>
            <div class="social-footer-right">
                <div >
                        &copy; 2019 <a href="https://landaurostov.ru" target="_blank">Юридическая фирма Ландау и Ко</a>
                </div>
                <div >
                        Разработка: <a href="https://lazarev-dmitry.ru" target="_blank">Дмитрий Лазарев</a>
                </div>
            </div>
            <div class="social-footer-icons">
                <!-- <ul class="menu simple">
                      <li><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li><a href="https://www.instagram.com/?hl=en"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                      <li><a href="https://www.pinterest.com/"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                      <li><a href="https://twitter.com/?lang=en"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                </ul> -->
            </div>
        </footer>
        <script src="/js/app.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.5.3/dist/js/foundation.min.js" ></script>
        <script>
            $(document).ready(function(){

                $("#tabs").tabs();
                $("#tabs>button").click(function(event){
                    $(this).parent().toggleClass('hidesidebar',{
                        duration:500,
                        children:true
                    });
                });
                $('.preloader').remove();
            });
            $(document).foundation();
        </script>
        <?php if (env('YANDEX_METRIKA_ID') !=''){?>
        <!-- Yandex.Metrika counter -->
    	<script type="text/javascript" >
    	   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
    	   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    	   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    	   ym(<?php echo env('YANDEX_METRIKA_ID'); ?>, "init", {
    		clickmap:true,
    		trackLinks:true,
    		accurateTrackBounce:true
    	   });
    	</script>
    	<noscript><div><img src="https://mc.yandex.ru/watch/<?php echo env('YANDEX_METRIKA_ID'); ?>" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    	<!-- /Yandex.Metrika counter -->
        <?php } ?>
        <?php if (env('RATING_MAIL_ID') !=''){?>
            <!-- Rating Mail.ru counter -->
            <script type="text/javascript">
            var _tmr = window._tmr || (window._tmr = []);
            _tmr.push({id: "<?php echo env('RATING_MAIL_ID');?>", type: "pageView", start: (new Date()).getTime()});
            (function (d, w, id) {
              if (d.getElementById(id)) return;
              var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
              ts.src = "https://top-fwz1.mail.ru/js/code.js";
              var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
              if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
            })(document, window, "topmailru-code");
            </script><noscript><div>
            <img src="https://top-fwz1.mail.ru/counter?id=<?php echo env('RATING_MAIL_ID');?>;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
            </div></noscript>
            <!-- //Rating Mail.ru counter -->

        <?php } ?>
        <script src="/js/node_modules/petrovich/dist/petrovich.min.js"></script>
    </body>
</html>
