<!DOCTYPE html>
<html lang="en" dir="ltr" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Юридическая фирма Ландау и К. Заявления в суд</title>
        <meta name="description" content="Юридическая фирма Ландау и К. Заявления в суд">
        <meta name="keywords" content="">
        <?php
            if (env('IS_LOCAL') == 'true'){
                require_once('local.php');
            } else {
                require_once('remote.php');
            }
         ?>
    </head>
    <body>
        <div class="preloader">
            <div class="container">
              <div class="loader">
                <div class="loader--dot"></div>
                <div class="loader--dot"></div>
                <div class="loader--dot"></div>
                <div class="loader--dot"></div>
                <div class="loader--dot"></div>
                <div class="loader--dot"></div>
                <div class="loader--text"></div>
              </div>
            </div>
        </div>
        <ul class="ui menu ">
            <a href="/"><img src="/images/logo.png" alt="Юридическая фирма Ландау и К. Заявления в суд"></a>
        </ul>
