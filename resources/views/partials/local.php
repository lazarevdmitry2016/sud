<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.5.3/dist/css/foundation.min.css">
<link href="https://fonts.googleapis.com/css?family=PT+Serif&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/js/node_modules/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/pepper-grinder/jquery-ui.min.css">
        <link rel="stylesheet" href="/css/pepper-grinder/theme.css">
        <link rel="stylesheet" href="/css/style.css">
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="/js/node_modules/jquery/dist/jquery.min.js"></script>
        <script src="/js/jquery-ui/jquery-ui.min.js"></script>
