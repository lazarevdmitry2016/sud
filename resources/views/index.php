<?php
require_once('partials/header.php');
?>
<div class="content text-center">
    <p>Юридическая фирма "Ландау и К" предлагает Вашему вниманию возможность заполнения различных заявлений в судебные инстанции прямо на нашем сайте.</p>
    <p>Здесь Вы сможете также скачать заполненные Вами заявления виде файлов различных форматов.</p>
</div>
<div id="app">
    <div class="aside">

    </div>
    <div class="content">
        <ul>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                    <a href="/document/0">Заявление о выдаче копии судебного постановления</a>
                </div>
            </li>

            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                    <a href="/document/1">Заявление о замене стороны правопреемником</a>
                </div>
            </li>
<?php /*            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/2"> Заявление о взыскании судебных расходов</a>
                </div>
            </li>
            */
?>

            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/3"> Заявление о выдаче исполнительного листа</a>
                </div>
            </li>

            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/4"> Заявление о выдаче копии аудиозаписи судебного заседания</a>
                </div>
            </li>
<?php       /*
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/5"> Заявление о выдаче копии судебного акта</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/6"> Заявление о вынесении дополнительного решения суда</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/7"> Заявление о пересмотре судебного постановления по гражданскому делу по вновь открывшимся обстоятельствам</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/8"> Заявление о предоставлении свидания с осужденным</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/9"> Заявление о прекращении производства по делу в связи с отказом от иска</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/10"> Заявление о приостановлении производства по делу</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/11"> Заявление о разъяснении решения суда</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/12"> Заявление о рассрочке исполнения решения суда</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/13"> Заявление об индексации присужденных денежных сумм</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/14"> Заявление об ознакомлении с делом/материалом</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/15"> Заявление об отмене заочного решения</a>
                </div>
            </li>
            <li class="radius bordered shadow card cell small-4 medium-4 large-2">
                <div class="card-section">
                <a href="/document/16"> Заявление об утверждении мирового соглашения и прекращении производства по делу</a>
                </div>
            </li>
*/
?>

        </ul>
    </div>
</div>

<script>
    data={};
</script>

<?php

require_once('partials/footer.php');
