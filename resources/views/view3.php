<?php
require_once('partials/header.php');
?>
<div id="app">
    <div class="sidebar sidebar-theme" id="tabs">
        <ul>
            <li><a href="#sud">Суд</a></li>
            <li><a href="#zayavitel">Заявитель</a></li>
            <li><a href="#postperson">Получатель</a></li>
        </ul>
        <button type="button" name="button" >
            <i class="fa fa-cog" aria-hidden="true"></i>
        </button>
        <form id="inputForm" action="/process" method="post">
            <input type="hidden" name="document" v-model="doc_num">
            <div id="sud">
                <sud-name-field
                    class="item"
                    v-model="sud_name"
                    name="sud_name"
                    case="t"
                    v-on:input-sud-name="sud_name_t = $event['out'];sud_name = $event['in']">
                </sud-name-field>
                <input type='hidden' name='sud_name_t' v-bind:value="sud_name_t"/>

                <date-field
                    v-model="isk_reshenie_date"
                    name="isk_reshenie_date"
                    title="Дата решения суда"
                    v-on:input-date="isk_reshenie_date = $event">
                </date-field>

                <text-field
                    v-model="isk_result"
                    name="isk_result"
                    title="Результат решения суда"
                    v-on:input-text="isk_result = $event">
                </text-field>

                <date-field
                    v-model="zakonnaya_sila_date"
                    name="zakonnaya_sila_date"
                    title="Дата вступления в законную силу"
                    v-on:input-date="zakonnaya_sila_date = $event">
                </date-field>
            </div>
            <div id="zayavitel">
                <name-field
                    v-model="zayavitel_name"
                    name="zayavitel_name"
                    title="Ф.И.О. заявителя (именительный падеж)"
                    v-on:input-name="zayavitel_name = $event['in'];gender=$event['out']['gender']">
                </name-field>
                <input type='hidden' name='gender' v-bind:value="gender" />

                <address-field
                    name="zayavitel_address_type"
                    v-model="zayavitel_address_type"
                    v-on:address-change="zayavitel_address_type = $event">
                </address-field>

                <text-field
                    v-model="zayavitel_address"
                    name="zayavitel_address"
                    title="Адрес заявителя"
                    v-on:input-text="zayavitel_address = $event">
                </text-field>

                <tel-field
                    v-model="zayavitel_phone"
                    name="zayavitel_phone"
                    title="Телефон заявителя"
                    v-on:input-tel="zayavitel_phone = $event">
                </tel-field>

                <email-field
                    v-model="zayavitel_email"
                    name="zayavitel_email"
                    title="E-mail заявителя"
                    v-on:input-email="zayavitel_email = $event">
                </email-field>
            </div>
            <div id="postperson">
                <name-field
                    v-model="post_person"
                    name="post_person"
                    title="Наименование/Ф.И.О. получателя"
                    v-on:input-name="post_person = $event['in'];post_person_d=$event['out']['d']">
                </name-field>
                <input type='hidden' name='post_person_d' v-bind:value="post_person_d" />

                <text-field
                    v-model="post_address"
                    name="post_address"
                    title="Адрес получателя"
                    v-on:input-text="post_address = $event">
                </text-field>


            </div>
            <?php include_once('partials/form-submit.php'); ?>
        </form>
    </div>
    <div class="content">
        <div class="a4-form">
            <div class="shapka">
                <p>В {{ sud_name ? sud_name : "(Название суда)"}} </p>
                <p>Заявитель: {{ zayavitel_name ? zayavitel_name : "(Ф.И.О. заявителя)"}}</p>
                <p>Адрес {{ zayavitel_address_type ? zayavitel_address_type : "(Тип адреса)"}}:{{ zayavitel_address ? zayavitel_address : "(Адрес заявителя)"}}</p>
                <p v-if="zayavitel_phone">Телефон: {{ zayavitel_phone }}</p>
                <p v-if="zayavitel_email">E-mail: {{ zayavitel_email }}</p>
            </div>
            <div class="zagolovok">
                <h1>Заявление <br>о выдаче исполнительного листа</h1>
            </div>
            <div class="text">
                <p>{{ isk_reshenie_date ? isk_reshenie_date : "(Дата вынесения решения)" }} {{ sud_name_t ? sud_name_t : "(Название суда)"}} было вынесено судебное постановление о {{ isk_result ? isk_result : "(Результат решения суда)" }}.</p>
                <p>Судебное постановление вступило в законную силу {{ zakonnaya_sila_date ? zakonnaya_sila_date : "(Дата вступления в законную силу)"}}.</p>
                <p>На основании изложенного и, руководствуясь статьей 428 Гражданского процессуального кодекса РФ, прошу выдать исполнительный лист на принудительное исполнение указанного  судебного постановления либо исполнительный лист направить {{ post_person ? post_person : "(Кому направить?)"}}  по адресу: {{ post_address ? post_address : "(Куда направить?)"}}.</p>
            </div>
            <div class="podval">
                <div class="container">
                    <div class="grid-x">
                        <div class="large-6 medium-6 small-6 ">«____» __________20__г.</div>
                        <div class="large-6 medium-6 small-6 ">
                            <div class="text-right">_______________</div>
                            <div class="text-right">(подпись)</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    <script>
        data={
            name:'',
            doc_num:'3',
            sud_name:'',
            sud_name_t:'',
            gender:'',
            zayavitel_name:'',
            zayavitel_address_type:'',
            zayavitel_address:'',
            zayavitel_phone:'',
            zayavitel_email:'',
            isk_reshenie_date:'',
            isk_result:'',
            zakonnaya_sila_date:'',
            post_person:'',
            post_person_d:'',
            post_address:''
        };
    </script>
<?php
require_once('partials/footer.php');
