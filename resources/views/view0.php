<?php
require_once('partials/header.php');
?>
    <div id="app">
        <div class="aside" id="tabs">
            <ul>
                <li><a href="#sud">Суд</a></li>
                <li><a href="#zayavitel">Заявитель</a></li>
                <li><a href="#zayavlenie">Заявление</a></li>
            </ul>
            <button type="button" name="button" >
                <i class="fa fa-cog" aria-hidden="true"></i>
            </button>
            <form id="inputForm" action="/process" method="post" class="ui grid">
                <input type="hidden" name="document" v-model="doc_num">
                <!-- Суд -->
                <div id="sud" >

                    <sud-name-field
                        class="item"
                        v-model="sud_name"
                        name="sud_name"
                        case="r"
                        v-on:input-sud-name="sud_name_r = $event['out'];sud_name = $event['in']">
                    </sud-name-field>
                    <input type='hidden' name='sud_name_r' v-bind:value="sud_name_r"/>

                    <date-field
                        v-model="sud_date"
                        name="sud_date"
                        title="Дата вынесения постановления"
                        v-on:input-date="sud_date = $event">
                    </date-field>

                    <name-field
                        v-model="sudya_name"
                        name="sudya_name"
                        title="Ф.И.О. судьи"
                        v-on:input-name="sudya_name_t = $event['out']['t'];sudya_name=$event['in']">
                    </name-field>
                    <input type='hidden' name='sudya_name_t' v-bind:value="sudya_name_t"/>

                    <postanovlenie-field
                        v-model="postanovlenie_type"
                        name="postanovlenie_type"
                        label="Тип постановления"
                        v-on:postanovlenie-change="postanovlenie_type = $event">
                    </postanovlenie-field>

                    <text-field
                        v-model="delo_num"
                        name="delo_num"
                        title="№ дела"
                        v-on:input-text="delo_num = $event">
                    </text-field>

                </div>
                <!-- /Суд -->
                <!-- Заявитель -->
                <div id="zayavitel">
                    <name-field
                        v-model="zayavitel_name"
                        name="zayavitel_name"
                        title="Ф.И.О. заявителя (именительный падеж)"
                        v-on:input-name="zayavitel_name = $event['in'];zayavitel_name_i = $event['out']['i'];zayavitel_name_r = $event['out']['r'];zayavitel_name_d = $event['out']['d']">
                    </name-field>
                    <input type='hidden' name='zayavitel_name_i' v-bind:value="zayavitel_name_i" />

                    <div class='form-group'>
                        <label>Выберите пол:</label>
                        <label>Мужской <input type="radio" name="pol" value="male" v-on:click="pol = 1" checked></label>
                        <label>Женский <input type="radio" name="pol" value="female" v-on:click="pol = 0"></label><br />
                        <input type="hidden" name="pol">
                    </div>

                    <address-field
                        name="zayavitel_address_type"
                        v-model="zayavitel_address_type"
                        v-on:address-change="zayavitel_address_type = $event">
                    </address-field>

                    <text-field
                        v-model="zayavitel_address"
                        name="zayavitel_address"
                        title="Адрес заявителя"
                        v-on:input-text="zayavitel_address = $event">
                    </text-field>

                    <tel-field
                        v-model="zayavitel_phone"
                        name="zayavitel_phone"
                        title="Контактный телефон заявителя"
                        v-on:input-tel="zayavitel_phone = $event">
                    </tel-field>

                    <email-field
                        v-model="zayavitel_email"
                        name="zayavitel_email"
                        title="E-mail заявителя"
                        v-on:input-email="zayavitel_email = $event">
                    </email-field>

                </div>
                <!-- /Заявитель -->
                <!-- Заявление -->
                <div id="zayavlenie">

                    <select-field
                        name="zayavlenie_type"
                        label="Тип заявления истца"
                        v-model="zayavlenie_type"
                        v-on:select-change="zayavlenie_type = $event">
                    </select-field>

                    <status-field
                        name="zayavitel_polozhenie"
                        label="Процессуальное положение заявителя"
                        v-model="zayavitel_polozhenie"
                        v-on:select-change="zayavitel_polozhenie = $event">
                    </status-field>

                    <name-field
                        v-if="zayavitel_polozhenie !== 'истцом'"
                        v-model="istetz_name"
                        name="istetz_name"
                        title="Наименование/Ф.И.О. истца"
                        v-on:input-name="istetz_name = $event['in'];istetz_name_r = $event['out']['r'];changeProcessState()">
                    </name-field>
                    <input
                        v-else
                        type='hidden'
                        name='istetz_name'
                        v-model="istetz_name"
                        />
                    <input type='hidden' name='istetz_name_r' v-bind:value="istetz_name_r" />

                    <name-field
                        v-if="zayavitel_polozhenie !== 'ответчиком'"
                        v-model="otvetchik_name"
                        name="otvetchik_name"
                        title="Наименование/Ф.И.О. ответчика"
                        v-on:input-name="otvetchik_name = $event['in'];otvetchik_name_d = $event['out']['d'];changeProcessState()">
                    </name-field>
                    <input
                        v-else
                        type='hidden'
                        name='otvetchik_name'
                        v-model="otvetchik_name" />
                    <input type='hidden' name='otvetchik_name_d' v-bind:value="otvetchik_name_d" />

                    <text-field
                        v-model="predmet_zayavleniya"
                        name="predmet_zayavleniya"
                        title="Предмет заявления"
                        v-on:input-text="predmet_zayavleniya = $event">
                    </text-field>

                </div>
                <!-- /Заявление -->
                <?php include_once('partials/form-submit.php'); ?>
            </form>

        </div>
        <div class="content ">
            <div class="a4-form">
                <div class="shapka">
                    <p>В {{ sud_name? sud_name : "(Название суда)" }}</p>
                    <p>Заявитель: {{ zayavitel_name_i ? zayavitel_name_i : "(Ф.И.О. заявителя)" }}</p>
                    <p>Адрес {{ zayavitel_address_type ? zayavitel_address_type : "(Тип адреса)" }}: {{ zayavitel_address ? zayavitel_address : "(Адрес заявителя)" }}</p>
                    <p v-if="zayavitel_phone">Телефон: {{ zayavitel_phone }}</p>
                    <p v-if="zayavitel_email">E-mail: {{ zayavitel_email }}</p>
                </div>
                <div class="zagolovok">
                    <h1>Заявление <br>о выдаче копии судебного постановления</h1>
                </div>
                <div class="text">
                    <p>{{ sud_date ? sud_date : "(Дата вынесения судебного постановления)" }} судьей {{ sud_name_r ? sud_name_r : "(Название суда)"}} {{ sudya_name_t ? sudya_name_t : "(Ф.И.О. судьи)" }} принято {{ postanovlenie_type ? postanovlenie_type : "(Тип постановления)" }}  по делу N {{ delo_num ? delo_num : "(Номер дела)" }} по {{ zayavlenie_type ? zayavlenie_type : "(Тип заявления)"}} {{ istetz_name_r ? istetz_name_r : "(Ф.И.О. истца)" }}  к {{ otvetchik_name_d ? otvetchik_name_d : "(Ф.И.О. ответчика)"}}  о {{ predmet_zayavleniya ? predmet_zayavleniya : "(Предмет заявления)" }}, по которому я являюсь {{ zayavitel_polozhenie ? zayavitel_polozhenie : "(Процессуальное положение заявителя)"}}.</p>
                    <p>Прошу выдать копию указанного судебного постановления.</p>
                </div>
                <div class="podval">
                    <div class="container">
                        <div class="grid-x">
                            <div class="large-6 medium-6 small-6 ">«____» __________20__г.</div>
                            <div class="large-6 medium-6 small-6 ">
                                <div class="text-right">_______________</div>
                                <div class="text-right">(подпись)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        data={
            name:'',
            doc_num:0,
            select:0,
            sud_name:'',
            zayavitel_name:'',
            zayavitel_name_i:'',
            zayavitel_name_r:'',
            zayavitel_name_d:'',
            pol:1,
            zayavitel_address_type:'',
            zayavitel_address:'',
            zayavitel_phone:'',
            zayavitel_email:'',
            sud_date:'',
            sud_name_r:'',
            sudya_name:'',
            sudya_name_t:'',
            postanovlenie_type:'',
            delo_num:'',
            zayavlenie_type:'',
            istetz_name:'',
            istetz_name_r:'',
            otvetchik_name:'',
            otvetchik_name_d:'',
            predmet_zayavleniya:'',
            zayavitel_polozhenie:''
        };
    </script>

<?php
require_once('partials/footer.php');
