<?php
require_once('partials/header.php');
?>
<div id="app">

        <div class="sidebar sidebar-theme" id="tabs">
            <ul class="sidebar-theme">
                <li><a href="#sud">Суд</a></li>
            </ul>
            <button type="button" name="button" >
                <i class="fa fa-cog" aria-hidden="true"></i>
            </button>
            <form id="inputForm" action="/process" method="post">
                <input type="hidden" name="document" v-model="doc_num">
                <div id="sud">
                    <sud-name-field
                        class="item"
                        v-model="sud_name"
                        name="sud_name"
                        case="r"
                        v-on:input-sud-name="sud_name_r = $event['out'];sud_name = $event['in']">
                    </sud-name-field>
                    <input type='hidden' name='sud_name_r' v-bind:value="sud_name_r"/>

                    <name-field
                        v-model="zayavitel_name"
                        name="zayavitel_name"
                        title="Ф.И.О. заявителя (именительный падеж)"
                        v-on:input-name="zayavitel_name = $event['in'];zayavitel_name_r = $event['out']['r'];">
                    </name-field>
                    <input type='hidden' name='zayavitel_name_r' v-bind:value="zayavitel_name_r"/>


                    <text-field
                        v-model="zayavitel_address"
                        name="zayavitel_address"
                        title="Адрес заявителя"
                        v-on:input-text="zayavitel_address = $event">
                    </text-field>

                    <tel-field
                        v-model="zayavitel_phone"
                        name="zayavitel_phone"
                        title="Телефон заявителя"
                        v-on:input-tel="zayavitel_phone = $event">
                    </tel-field>

                    <!-- <status-field
                        name="zayavitel_status"
                        label="Процессуальный статус заявителя"
                        v-model="zayavitel_status"
                        v-on:select-change="zayavitel_status = $event">
                    </status-field> -->

                    <text-field
                        v-model="passport_data"
                        name="passport_data"
                        title="Паспортные данные "
                        v-on:input-text="passport_data = $event">
                    </text-field>

                    <text-field
                        v-model="delo_num"
                        name="delo_num"
                        title="Номер дела "
                        v-on:input-text="delo_num = $event">
                    </text-field>


                </div>
                <?php include_once('partials/form-submit.php'); ?>
            </form>
        </div>
        <div class="content">
            <div class="a4-form">
                <div class="shapka">
                    <p>Председателю {{ sud_name_r ? sud_name_r : "(Название суда)" }}</p>
                    <p>от  {{ zayavitel_name_r ? zayavitel_name_r : "(Ф.И.О. заявителя)" }},</p>
                    <p>паспорт {{ passport_data ? passport_data : "(Паспортные данные)" }},</p>
                    <p>проживающего по адресу: {{ zayavitel_address ? zayavitel_address : "(Адрес заявителя)" }},</p>
                    <p>номер контактного телефона {{ zayavitel_phone ? zayavitel_phone : "(№ телефона заявителя)" }}</p>
                </div>
                <div class="zagolovok">
                    <h1>Заявление <br>о выдаче копии аудиозаписи судебного заседания</h1>
                </div>
                <div class="text">
                    <p>1. Прошу выдать мне копию(ии) аудиозаписи судебного заседания по материалу/делу {{ delo_num ? delo_num : "(№ дела)" }}.</p>

                    <p>
                        <div class="large-6 medium-6 small-6">
                            Подпись/Ф.И.О. (расшифровать):
                        </div>
                        <div class="large-6 medium-6 small-6">
                            “_____” _____________ 20    г.
                        </div>
                    </p>
                    <p>2. Электронный носитель информации прилагается.</p>
                    <p>3. Копию электронного носителя получил
                    (Ф.И.О. и подпись лица, получившего копию,
                    дата выдачи копии)_____________________________________________________________.</p>
                    <p>4. Выдал
                    (должность, Ф.И.О., подпись работника аппарата суда, выдавшего электронный носитель, дата)_____________________________________________________________.</p>
                </div>
                <div class="podval">
                    <div class="container">
                        <div class="grid-x">
                            <div class="large-6 medium-6 small-6 ">«____» __________20__г.</div>
                            <div class="large-6 medium-6 small-6 ">
                                <div class="text-right">_______________</div>
                                <div class="text-right">(подпись)</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
</div>
<script>
    data={
        name:'',
        doc_num:'4',
        sud_name:'',
        sud_name_r:'',
        zayavitel_name:'',
        zayavitel_name_r:'',
        zayavitel_address:'',
        zayavitel_phone:'',
        // zayavitel_status:'',
        passport_data:'',
        delo_num:''
    };
</script>
<?php
require_once('partials/footer.php');
