<?php
require_once('partials/header.php');
?>
    <div id="app">
        <div class="aside" id="tabs">
            <ul>
                <li><a href="#sud">Суд</a></li>
                <li><a href="#zayavitel">Заявитель</a></li>
                <li><a href="#delo">Дело</a></li>
            </ul>
            <button type="button" name="button" >
                <i class="fa fa-cog" aria-hidden="true"></i>
            </button>
            <form id="inputForm" action="/process" method="post">
                <input type="hidden" name="document" v-model="doc_num">
                <!-- Суд -->
                <div id="sud">
                    <sud-name-field
                        class="item"
                        v-model="sud_name"
                        name="sud_name"
                        case="r"
                        v-on:input-sud-name="sud_name_r = $event['out'];sud_name = $event['in']">
                    </sud-name-field>
                    <input type='hidden' name='sud_name_r' v-bind:value="sud_name_r"/>

                    <text-field
                        v-model="sud_delo"
                        name="sud_delo"
                        title="№ дела"
                        v-on:input-text="sud_delo = $event">
                    </text-field>

                </div>
                <!-- /Суд -->
                <!-- Заявитель -->
                <div id="zayavitel">
                    <name-field
                        v-model="zayavitel_name"
                        name="zayavitel_name"
                        title="Ф.И.О. заявителя"
                        case="i"
                        v-on:input-name="zayavitel_name = $event['in'];zayavitel_name_i = $event['out']['i'];zayavitel_name_r = $event['out']['r'];zayavitel_name_d = $event['out']['d'];pol=($event['gender']==1)?'являвшийся':'являвшаяся'">
                    </name-field>
                    <input type='hidden' name='gender' v-bind:value="gender" />
                    <input type='hidden' name='pol' v-bind:value="pol" />
                    <input type='hidden' name='zayavitel_name_i' v-bind:value="zayavitel_name_i" />

                    <address-field
                        name="zayavitel_address_type"
                        v-model="zayavitel_address_type"
                        v-on:address-change="zayavitel_address_type = $event"
                    ></address-field>

                    <text-field
                        v-model="zayavitel_address"
                        name="zayavitel_address"
                        title="Адрес заявителя"
                        v-on:input-text="zayavitel_address = $event">
                    </text-field>

                    <status-field
                        name="zayavitel_polozhenie"
                        label="Процессуальное положение заявителя"
                        v-model="zayavitel_polozhenie"
                        v-on:select-change="zayavitel_polozhenie = $event"
                    ></status-field>

                    <text-field
                        v-model="prichina"
                        name="prichina"
                        title="Причина замены"
                        v-on:input-text="prichina = $event">
                    </text-field>

                    <tel-field
                        v-model="zayavitel_phone"
                        name="zayavitel_phone"
                        title="Контактный телефон заявителя"
                        v-on:input-tel="zayavitel_phone = $event">
                    </tel-field>

                    <email-field
                        v-model="zayavitel_email"
                        name="zayavitel_email"
                        title="E-mail заявителя"
                        v-on:input-email="zayavitel_email = $event">
                    </email-field>

                </div>
                <!-- /Заявитель -->
                <!-- Дело -->
                <div id="delo">

                    <name-field
                        v-if="zayavitel_polozhenie !== 'истцом'"
                        v-model="istetz_name"
                        name="istetz_name"
                        title="Наименование/Ф.И.О. истца"
                        v-on:input-name="istetz_name = $event['in'];istetz_name_r = $event['out']['r'];changeProcessState()">
                    </name-field>
                    <input
                        v-else
                        type='hidden'
                        name='istetz_name'
                        v-model="istetz_name"
                        />
                    <input type='hidden' name='istetz_name_r' v-bind:value="istetz_name_r" />

                    <name-field
                        v-if="zayavitel_polozhenie !== 'ответчиком'"
                        v-model="otvetchik_name"
                        name="otvetchik_name"
                        title="Наименование/Ф.И.О. ответчика"
                        v-on:input-name="otvetchik_name = $event['in'];otvetchik_name_d = $event['out']['d'];changeProcessState()">
                    </name-field>
                    <input
                        v-else
                        type='hidden'
                        name='otvetchik_name'
                        v-model="otvetchik_name" />
                    <input type='hidden' name='otvetchik_name_d' v-bind:value="otvetchik_name_d" />

                    <text-field
                        v-model="predmet_zayavleniya"
                        name="predmet_zayavleniya"
                        title="Предмет заявления"
                        v-on:input-text="predmet_zayavleniya = $event">
                    </text-field>

                    <name-field
                        v-model="pravopreemnik_name"
                        name="pravopreemnik_name"
                        title="Наименование/Ф.И.О. правопреемника"
                        case="t"
                        v-on:input-name="pravopreemnik_name = $event['in'];pravopreemnik_name_t = $event['out']['t']">
                    </name-field>
                    <input type='hidden' name='pravopreemnik_name_t' v-bind:value="pravopreemnik_name_t" />

                </div>
                <!-- /Дело -->
                <?php include_once('partials/form-submit.php'); ?>
            </form>
        </div>
        <div class="content">
            <div class="a4-form">
                <div class="shapka">
                    <p>В {{ sud_name ? sud_name :"(Название суда)" }} </p>
                    <p>Заявитель: {{ zayavitel_name_i ? zayavitel_name_i : "(Ф.И.О. заявителя)" }}</p>
                    <p>Адрес {{ zayavitel_address_type ? zayavitel_address_type : "(Тип адреса)" }}:{{ zayavitel_address ? zayavitel_address :"(Адрес заявителя)" }}</p>
                    <p v-if="zayavitel_phone">Телефон: {{ zayavitel_phone }}</p>
                    <p v-if="zayavitel_email">E-mail: {{ zayavitel_email }}</p>
                    <p>по делу по иску № {{ sud_delo ? sud_delo : "(№ дела)" }}</p>
                </div>
                <div class="zagolovok">
                    <h1>Заявление <br>о замене стороны правопреемником</h1>
                </div>
                <div class="text">
                    <p>
                        В производстве суда находится дело по иску {{ istetz_name_r ? istetz_name_r :"(Ф.И.О. истца)" }} к {{ otvetchik_name_d ? otvetchik_name_d : "(Ф.И.О. ответчика)" }} о {{ predmet_zayavleniya ? predmet_zayavleniya : "(Предмет заявления)" }}.
                    </p>
                    <p>
                        В связи с {{ prichina ? prichina : "(Причина)" }} {{ zayavitel_name_i ? zayavitel_name_i :"(Ф.И.О. заявителя)" }}, {{ pol? pol : "(являвшийся / являвшаяся)" }} {{ zayavitel_polozhenie ? zayavitel_polozhenie : "(Процессуальное положение заявителя)" }}, выбывает из спорного/установленного решением суда правоотношения.
                    </p>
                    <p>
                        На основании статьи 44 Гражданского процессуального кодекса РФ прошу суд заменить выбывшую сторону ее правопреемником {{ pravopreemnik_name_t ? pravopreemnik_name_t :"(Ф.И.О. правопреемника)" }}.
                    </p>
                    <p>Приложение: Документы, подтверждающие правопреемство.</p>
                </div>
                <div class="podval">
                    <div class="container">
                        <div class="grid-x">
                            <div class="large-6 medium-6 small-6 ">«____» __________20__г.</div>
                            <div class="large-6 medium-6 small-6 ">
                                <div class="text-right">_______________</div>
                                <div class="text-right">(подпись)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        data={
            name:'',
            doc_num:1,
            sud_name:'',
            sud_name_r:'',
            sud_delo:'',
            zayavitel_name:'',
            zayavitel_name_i:'',
            zayavitel_name_r:'',
            zayavitel_name_d:'',
            zayavitel_address_type:'',
            zayavitel_polozhenie:'',
            zayavitel_address:'',
            prichina:'',
            zayavitel_phone:'',
            zayavitel_email:'',
            istetz_name:'',
            istetz_name_r:'',
            otvetchik_name:'',
            otvetchik_name_d:'',
            predmet_zayavleniya:'',
            pravopreemnik_name:'',
            pravopreemnik_name_t:'',
            pol:'',
            gender:'male'
        };
    </script>
<?php
require_once('partials/footer.php');
