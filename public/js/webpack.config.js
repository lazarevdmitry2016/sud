const path = require('path');

module.exports = {
  entry: './init.js',
  mode:'production',
  output: {
    filename: 'app.js',
    path:'/shared/lumen/sud/sud/public/js'
  }
};
