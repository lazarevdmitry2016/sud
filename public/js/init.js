import   './vue/TextField';
import   './vue/NameField';
import   './vue/SudNameField';
import   './vue/TelField';
import   './vue/EmailField';
import   './vue/DateField';
import   './vue/OptionField';
import   './vue/AddressField';
import   './vue/SelectField';
import   './vue/StatusField';
import   './vue/PostanovlenieField';
import   './vue/NumberField';

import './vue/App';
