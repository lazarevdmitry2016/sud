$(document).ready(function(){
    $("input[name='sud_name']").val("Ворошиловский районный суд г. Ростова-на-Дону");

    $("input[name='sud_name_r']").val("Ворошиловского районного суда г. Ростова-на-Дону");
    $("input[name='sud_date']").val("2019-01-01");
    $("input[name='sudya_name']").val("Иванов Иван Иванович");
    $("input[name='sudya_name_t']").val("Ивановым Иваном Ивановичем");
    $("select[name='postanovlenie_type']").val("решение");
    app.postanovlenie_type="решение";
    $("input[name='delo_num']").val("2-1234/2019");
    $("input[name='zayavitel_name']").val("Петров Петр Петрович");
    $("input[name='zayavitel_name_i']").val("Петров Петр Петрович");
    $("input[name='pol']").val("male"); // !!!
    app.pol=1;
    $("select[name='zayavitel_address_type']").val("проживания");
    app.zayavitel_address_type="проживания";
    $("input[name='zayavitel_address']").val("344000, г. Ростов-на-Дону, ул. Б. Садовая, д. 1");
    $("input[name='zayavitel_phone']").val("+78634000000");
    $("input[name='zayavitel_email']").val("admin@admin.adm");
    $("select[name='zayavlenie_type']").val("иску");
    app.zayavlenie_type="иску";
    $("select[name='zayavitel_polozhenie']").val("третьим лицом");
    app.zayavitel_polozhenie="третьим лицом";
    $("input[name='istetz_name']").val("Сидоров Михаил Александрович");
    $("input[name='istetz_name_r']").val("Сидорова Михаила Александровича");
    $("input[name='otvetchik_name']").val("Александрова Анна Петровна");
    $("input[name='otvetchik_name_d']").val("Александровой Анне Петровне");
    $("input[name='predmet_zayavleniya']").val("предмет заявления");
});
