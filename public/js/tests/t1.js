(function(){
    app.sud_name="Ворошиловский районный суд г. Ростова-на-Дону";
    app.sud_name_rod="Ворошиловского районного суда г. Ростова-на-Дону";
    app.delo_num="2-1234/2019";
    app.zayavitel_fio="Петров Петр Петрович";
    app.zayavitel_fio_im="Петров Петр Петрович";
    app.zayavitel_address_type="проживания";
    app.zayavitel_address="344000, г. Ростов-на-Дону, ул. Б. Садовая, д. 1";
    app.zayavitel_phone="+78634000000";
    app.zayavitel_email="admin@admin.adm";
    app.zayavitel_polozhenie="третьим лицом";
    app.prichina='причине';
    app.istetz_fio="Сидоров Михаил Александрович";
    app.istetz_fio_rod="Сидорова Михаила Александровича";
    app.otvetchik_fio="Александрова Анна Петровна";
    app.otvetchik_fio_dat="Александровой Анне Петровне";
    app.predmet_zayavleniya="предмет заявления";
    app.pravopreemnik_name='Петрова Ирина Витальевна';
    app.pravopreemnik_name_tv='Петровой Ириной Витальевной';
    app.pol=1;
    app.gender='male';

})()
