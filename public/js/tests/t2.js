(function(){
    app.sud_name="Ворошиловский районный суд г. Ростова-на-Дону";
    app.zayavitel_name="Петров Петр Петрович";
    app.zayavitel_status='';
    app.zayavitel_address_type="проживания";
    app.zayavitel_address="344000, г. Ростов-на-Дону, ул. Б. Садовая, д. 1";
    app.zayavitel_phone="+78634000000";
    app.zayavitel_email="admin@admin.adm";
    app.predmet_zayavleniya="предмет заявления";
    app.storona_name='Александрова Анна Петровна';
    app.storona_name_rod='Александровой Анны Петровны';
    app.storona_address_type='проживания';
    app.storona_address='344000, г. Ростов-на-Дону, ул. Б. Садовая, д. 101';
    app.storona_phone='+78635000000';
    app.storona_email='alex@alex.al';
    app.istetz_name="Сидоров Михаил Александрович";
    app.otvetchik_name_dat='';
    app.isk_predmet='предмет иска';
    app.isk_reshenie_date='2019-09-01';
    app.isk_result='';
    app.raskhody_vid='';
    app.raskhody_razmer='';
    app.raskhody_vsego='';
})()
