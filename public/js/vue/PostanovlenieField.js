Vue.component('postanovlenie-field',{
    props:['name','label'],
    template:`
    <div class="form-control">
        <label  v-bind:for='name'>{{ label }}</label>
        <select
            class="form-control"
            v-bind:name="name"
            v-on:change="$emit('postanovlenie-change',$el.children[1].value)"
        >
            <option value="">Выберите из списка</option>
            <option value="решение">Решение</option>
            <option value="определение">Определение</option>
        </select>
    </div>
    `
});
