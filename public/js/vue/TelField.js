Vue.component('tel-field',{
    props:['name','title'],
    template:`
    <div class="form-control">
        <label  v-bind:for='name'>{{ title }}</label>
        <input type="tel" v-bind:name="name" v-on:input="$emit('input-tel',$el.children[1].value)">
    </div>
    `
});
