Vue.component('option-field',{
    props:['value','text'],
    template:`
        <option v-bind:value="value">{{ text }}</option>
    `
});
