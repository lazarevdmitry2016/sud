
/*
    Example:

    <number-field
        v-model="x"
        name="x"
        title="y"
        v-on:input-number="x = $event">
    </number-field>
*/
Vue.component('number-field',{
    props: ['name', 'title'],
    template: `
        <div class="form-control">
            <label  v-bind:for='name'>{{ title }}</label>
            <input type="number" v-bind:name="name" v-on:focusout="$emit('input-number',$el.children[1].value)">
        </div>
        `
});
