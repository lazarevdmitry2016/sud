Vue.component('address-field',{
    props:['name'],
    template:`
    <div class="form-control">
        <label v-bind:for='name'>Тип адреса</label>
        <select
            class="form-control"
            v-bind:name="name"
            v-on:change="$emit('address-change',$el.children[1].value)"
        >
            <option value="">Выберите из списка</option>
            <option value="проживания" >Адрес проживания</option>
            <option value="регистрации">Адрес регистрации</option>
        </select>
    </div>
    `
});
