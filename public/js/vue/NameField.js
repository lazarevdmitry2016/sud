/*
    Example:

    <name-field
        v-model="x"
        name="x"
        title="y"
        v-on:input-name="x = $event">
    </name-field>
*/
Vue.component('name-field', {
        props: ['name', 'title'],
        template: `
            <div class="form-control">
                <label  v-bind:for='name'>{{ title }}</label>
                <input type="text" v-bind:name="name" v-on:focusout="$emit('input-name',{'in':$el.children[1].value,'out':transformedName($el.children[1].value),'gender':detectGender($el.children[1].value)})" >
            </div>
            `,
        methods:{
            /*
                @param  name

                @return object
            */
            transformedName:function(name){
                var names=name.split(' ');
                var cases=[
                    'i','r','d','v','t','p'
                ];
                var result={};
                if (names && names.length==3){
                    var person={
                        gender:petrovich.detect_gender(names[2]),
                        last:names[0],
                        first:names[1],
                        middle:names[2]
                    };
                    var petr={
                        'i':petrovich(person,'nominative'),
                        'r':petrovich(person,'genitive'),
                        'd':petrovich(person,'dative'),
                        'v':petrovich(person,'accusative'),
                        't':petrovich(person,'instrumental'),
                        'p':petrovich(person,'prepositional')
                    }
                    for (var i=0;i<cases.length;i++){
                        var c=cases[i];
                        result[c]=petr[c].last+" "+petr[c].first+" "+petr[c].middle;
                    }

                } else {
                    for (var i=0;i<cases.length;i++){
                        var c=cases[i];
                        result[c]=name;
                    }
                }
                return result;
            },
            detectGender:function(n){
                var names=n.split(' ');
                if (names && names[2]){
                    var s=names[2];
                    gender=petrovich.detect_gender(s);
                    switch(gender){
                        case 'female':return 0;break;
                        case 'male':
                        default:return 1;break;
                    }
                } else {
                    return 1;
                }
            }
        }
});
