Vue.component('status-field',{
    props:['name','label'],
    template:`
    <div class="form-control">
        <label  v-bind:for='name'>{{ label }}</label>
        <select
            class="form-control"
            v-bind:name="name"
            v-on:change="$emit('select-change',$el.children[1].value)">
            <option value="">Выберите из списка</option>
            <option value="истцом">Истец</option>
            <option value="ответчиком">Ответчик</option>
            <option value="третьим лицом">Третье лицо</option>
        </select>
    </div>
    `
});
