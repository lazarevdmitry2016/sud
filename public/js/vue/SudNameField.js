/*
    Example:

    <sud-name-field
        v-model="x"
        name="x"
        v-on:input-sud-name="x = $event">
    </sud-name-field>
*/
Vue.component('sud-name-field', {
        props: ['name','case'],
        template: `
            <div class="form-control">
                <label v-bind:for='name'>Название суда</label>
                <input type="text" v-bind:name="name" v-on:focusout="$emit('input-sud-name',transformSudName($el.children[1].value))" >
            </div>
            `,
        methods:{
            /*
                @return object {name, transformed_name}
            */
            transformSudName:function(name){
                var words=name.split(' ');
                var result;
                if (words && words.length > 0){
                    var tranformedWords=[];
                    for (var i=0;i<words.length;i++){
                        word=words[i];
                        var tranformedWord;
                        if (word == 'Суд'){
                            switch (this.case) {
                                case 'r':tranformedWord='Суда';break;
                                case 'd':tranformedWord='Суду';break;
                                case 'v':tranformedWord='Суд';break;
                                case 't':tranformedWord='Судом';break;
                                case 'p':tranformedWord='Суде';break;
                                case 'i':
                                default:tranformedWord='Суд';break;
                            }
                            tranformedWords.push(tranformedWord);
                        } else if (word == 'суд'){
                            switch (this.case) {
                                case 'r':tranformedWord='суда';break;
                                case 'd':tranformedWord='суду';break;
                                case 'v':tranformedWord='суд';break;
                                case 't':tranformedWord='судом';break;
                                case 'p':tranformedWord='суде';break;
                                case 'i':
                                default:tranformedWord='суд';break;
                            }
                            tranformedWords.push(tranformedWord);
                        } else if (word.length > 3 && word[word.length-1] == 'й'){
                            var c;
                            switch (this.case) {
                                case 'r':c='genitive';break;
                                case 'd':c='dative';break;
                                case 'v':c='accusative';break;
                                case 't':c='instrumental';break;
                                case 'p':c='prepositional';break;
                                case 'i':
                                default:c='nominative';break;
                            }
                            tranformedWords.push(petrovich({
                                gender:'male',
                                last:word
                            },c).last);
                        } else {
                            tranformedWords.push(word);
                        }
                    }
                    var tranformedPhrase=tranformedWords.join(' ');
                    result={
                        in:name,
                        out:tranformedPhrase
                    };
                } else {
                    result={
                        in:name,
                        out:name
                    };
                }
                return result;
            },
            getCase:function(c){
                var out;
                switch (c) {
                    case 'r': out='genitive' ;break;
                    case 'd': out='dative' ;break;
                    case 'v': out='accusative' ;break;
                    case 't': out='instrumental' ;break;
                    case 'p': out='prepositional' ;break;
                    case 'i':
                    default:out='nominative';
                }
                return out;
            }
        }
});
