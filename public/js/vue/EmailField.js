Vue.component('email-field',{
    props:['name','title'],
    template:`
    <div class="form-control">
        <label  v-bind:for='name'>{{ title }}</label>
        <input type="email" v-bind:name="name" v-on:input="$emit('input-email',$el.children[1].value)">
    </div>
    `
});
