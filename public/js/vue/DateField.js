/*
    Example:

    <date-field
        v-model="x"
        name="x"
        title="y"
        v-on:input-date="x = $event">
    </date-field>
*/
Vue.component('date-field', {
    props: ['name', 'title'],
    template: `
        <div class="form-control">
            <label v-bind:for='name'>{{ title }}</label>
            <input type="date" v-bind:name="name" v-on:input="$emit('input-date',formatDate($el.children[1].value))">
        </div>
        `,
    methods:{
        formatDate:function(d){
            return (new Date(d)).toLocaleDateString();
        }
    }
});
