Vue.component('select-field',{
    props:['name','label','options'],
    template:`
    <div class="form-control">
        <label  v-bind:for='name'>{{ label }}</label>
        <select
            class="form-control"
            v-bind:name="name"
            v-on:change="$emit('select-change',$el.children[1].value)">
            <option value="">Выберите из списка</option>
            <option value="иску">Иск</option>
            <option value="заявлению">Заявление</option>
        </select>
    </div>
    `
});
