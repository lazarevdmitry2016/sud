Vue.component('text-field',{
    props:['name','title'],
    template:`
    <div class="form-control">
        <label v-bind:for='name'>{{ title }}</label>
        <input type="text" v-bind:name="name" v-on:focusout="$emit('input-text',$el.children[1].value)">
    </div>
    `
});
